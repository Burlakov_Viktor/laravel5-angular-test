var app = angular.module('taskApp', ['ngAnimate', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('taskController', function($scope, $http, $uibModal) {
    $scope.tasks = [];
    $scope.page = 1;
    $scope.q = '';
    $scope.totalPage = 0;
    $scope.loading = false;
    $scope.cachedTasks = [];

    $scope.init = function() {
        $scope.loading = true;
        $http.get('/api/v1/task', {params: {page: $scope.page, q: $scope.q}}).
            success(function(data, status, headers, config) {
                $scope.tasks = data.tasks;
                $scope.page = data.page;
                $scope.totalPage = data.totalPage;
                $scope.loading = false;
            });
    }

    $scope.setPage = function(page) {
        $scope.page = page;
        $scope.init();
    }

    $scope.search = function() {
        $scope.page = 1;
        $scope.init();
    }

    $scope.show = function(id) {
        if ($scope.cachedTasks[id]) {
            $scope.popup($scope.cachedTasks[id]);
        } else {
            $scope.loading = false;
            $http.get('/api/v1/task/' + id).
                success(function(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.cachedTasks[id] = data;
                    $scope.popup(data);
            });
        }
    }

    $scope.popup = function(task) {
         $uibModal.open({
             animation: false,
             templateUrl: 'modal.html',
             controller: 'ModalCtrl',
             size: 'lg',
             resolve: {
                 currentTask: function () {
                   return task;
                 }
             }
        });
    }

}).controller('ModalCtrl', function ($scope, $uibModalInstance, currentTask) {
    $scope.currentTask = currentTask;

    $scope.ok = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
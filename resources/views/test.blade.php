<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test</title>

        <!--AngularJS-->
        <script src="js/angular.1.5.3.js"></script>
        <script src="js/angular-animate.1.5.3.js"></script>
        <script src="js/ui-bootstrap-custom-1.3.3.js"></script>
        <script src="js/ui-bootstrap-custom-tpls-1.3.3.js"></script>

        <script src="js/app.js"></script>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/ui-bootstrap-custom-1.3.3-csp.css" rel="stylesheet">
    </head>
    <body>
        <div class="container" ng-app="taskApp" ng-controller="taskController" ng-init="init()">
            <!-- Popup window -->
            <script type="text/ng-template" id="modal.html">
                <div class="modal-header">
                    <h3 class="modal-title">Информация о задаче <% currentTask.id %></h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Заголовок</label>
                            <div class="col-sm-10">
                               <input type="text" class="form-control" ng-value="(currentTask.title)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Дата выполнения</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" ng-value="(currentTask.date)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Автор</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" ng-value="(currentTask.author)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Статус</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" ng-value="(currentTask.status)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Описание</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" ng-value="(currentTask.description)">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
                </div>
            </script>

            <h1>Test</h1>
            <div ng-show="loading">Loading...</div>
            <div class="row">
                <div class="col-md-6">
                    <form class="form-inline">
                        <input type="text" class="form-control" ng-model="q">
                        <button type="submit" class="btn btn-primary" ng-click="search()">Find</button>
                   </form>
                </div>
            </div>
            <br />
            <div class="row" ng-show="tasks.length">
                <div class="col-md-6">
                    <table class="table table-striped">
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date</th>
                        </tr>
                        <tr ng-repeat='task in tasks'>
                            <td><% task.id %></td>
                            <td><a href ng-click="show(task.id)"><% task.title %></a></td>
                            <td><% task.date %></td>
                        </tr>
                    </table>
                    <uib-pagination total-items="(totalPage)" ng-model="page" ng-click="setPage(page)" max-size="5" class="pagination-sm" boundary-link-numbers="true"></uib-pagination>
                </div>
            </div>
        </div>
    </body>
</html>

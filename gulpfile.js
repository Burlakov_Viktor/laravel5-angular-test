elixir(function(mix) {
    mix.sass('app.scss');
});

elixir(function(mix) {
    mix.browserify('app.js');
});

elixir(function(mix) {
    mix.version(['css/app.css', 'js/app.js']);
});
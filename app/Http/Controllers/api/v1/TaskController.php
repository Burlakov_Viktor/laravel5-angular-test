<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Task;

class TaskController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        return response()->json(Task::one($id));
    }

    /** Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $page = $request->page ?: 1;
        $q = $request->q ?: '';
        $collection = Task::all($page, $q);

        return response()->json([
            'tasks' => $collection['tasks'],
            'page' => $page,
            'totalPage' => $collection['count'],
        ]);
    }
}

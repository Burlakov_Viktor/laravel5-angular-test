<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class Task
{

    /**
     * Список задач
     * @param  integer $page Текущая страница
     * @param  string  $q    Строка поиска
     * @return []
     */
    public static function all($page = 1, $q = '')
    {
        $collection = collect(self::total());
        if ($q) {
            $collection = $collection->where('title', $q);
        }

        return [
            'tasks' => $collection->forPage($page, 10)->values(),
            'count' => $collection->count(),
        ];
    }

    /**
     * Одна задача
     * @param  integer $id ИД задачи
     * @return []
     */
    public static function one($id)
    {
         $collection = collect(self::total());

        return $collection->where('id', $id)->first();
    }

    /**
     * Генерация всех задач (+ кеш)
     * @return []
     */
    public static function total()
    {
        if (Cache::has('tasks')) {
            return Cache::get('tasks');
        }

        $tasks = [];
        for ($i = 1; $i <= 100; $i++) {
            $tasks[$i] = [
                'id' => $i,
                'title' => 'title '.$i,
                'date' => date('m-d-Y').' '.$i,
                'author' => 'author '.$i,
                'status' => 'status '.$i,
                'description' => 'description '.$i,
            ];
        }

        Cache::put('tasks', $tasks, 60);

        return $tasks;
    }
}
